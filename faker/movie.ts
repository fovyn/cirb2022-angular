const fs = require('fs');
const { faker } = require('@faker-js/faker');
type MovieType = 'movie' | 'series';
type MovieRate = 'PG-10' | 'PG-13' | 'PG-18';

class MovieRating {
    Source: string;
    Value: string;
}
class Movie {
    Id: string;
    Title: string;
    Year: string;
    Rated: MovieRate;
    Released: string;
    Runtime: string;
    Genre: string;
    Director: string;
    Writer: string;
    Actors: string;
    Plot: string;
    Language: string;
    Country: string;
    Awards: string;
    Poster: string;
    Ratings: Array<MovieRating>;
    Metascore: string;
    imdbRating: string;
    imdbVotes: string;
    imdbID: string;
    Type: MovieType;
    DVD: string;
    BoxOffice: string;
    Production: string;
    Website: string;
    Response: string
}

function createRandomMovie(): Movie {
    return {
        Id: faker.datatype.uuid(),
        Title: faker.music.songName(),
        Year: `${faker.date.between('1960-01-01', '2022-11-09')}`,
        Rated: faker.helpers.arrayElement(['PG-10', 'PG-13', 'PG-18']),
        Released: `${faker.date.between('1960-01-01', '2022-11-09')}`,
        Runtime: `${faker.datatype.number({ min: 100 })}`,
        Genre: `${faker.music.genre()}`,
        Director: faker.name.fullName(),
        Writer: faker.name.fullName(),
        Actors: faker.datatype.array(faker.datatype.number({ max: 20 })).join(','),
        Plot: faker.lorem.paragraph(2),
        Language: faker.helpers.arrayElement(['French', 'English']),
        Country: faker.address.country(),
        Awards: faker.datatype.array(faker.datatype.number({ max: 20 })).join(','),
        Poster: faker.image.imageUrl(),
        Ratings: faker.helpers.arrayElements(),
        Metascore: faker.datatype.number().toString(),
        imdbRating: faker.datatype.number().toString(),
        imdbVotes: faker.datatype.number().toString(),
        imdbID: faker.datatype.number().toString(),
        Type: faker.helpers.arrayElement(),
        DVD: faker.date.soon().toISOString(),
        BoxOffice: "N/A",
        Production: "N/A",
        Website: "N/A",
        Response: "N/A"
    }
}

const items = [];
for (let i = 0; i < 20; i++) {
    items.push(createRandomMovie());
}


fs.appendFileSync("./data.json", JSON.stringify(items));