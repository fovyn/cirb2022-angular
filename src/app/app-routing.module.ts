import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from "./modules/security/component/login/login.component";

export const AppRoutes: Routes = [
    { path: "", component: HomeComponent },
    { path: "login", component: LoginComponent },
    { path: 'movie', loadChildren: () => import('./pages/movie/movie.module').then(m => m.MovieModule) },
    { path: 'chrono', loadChildren: () => import('./pages/chrono/chrono.module').then(m => m.ChronoModule) }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AppRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }