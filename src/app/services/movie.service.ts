import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, map, Observable, Subscription } from 'rxjs';
import { environment } from './../../environments/environment';

export const initialState = {
    value: [],
    loading: false
}

@Injectable({
    providedIn: 'root'
})
export class MovieService {
    private state$: BehaviorSubject<{ value: any[], loading: boolean }> = new BehaviorSubject(initialState);

    get State$(): Observable<{ value: any[], loading: boolean }> { return this.state$.asObservable(); }

    subscribe(action: any): Subscription {
        return this.state$.subscribe(action);
    }

    constructor(private $http: HttpClient) { }

    changeState(data: Partial<{ value: any[], loading: boolean }>) {
        const value = this.state$.value;
        this.state$.next({ ...value, ...data });
    }


    GetGenres(page: number = 1) {
        this.changeState({ loading: true });
        const params = new HttpParams()
            .append("api_key", environment.api.key)
            .append("page", page);

        return this.$http.get<any>(`${environment.api.url}/genre/movie/list`, { params }).pipe(
            map(it => it.genres.map(el => el.name))
        ).subscribe((data: string[]) => {
            setTimeout(() => this.changeState({ value: data, loading: false }), 2000);
        })
    }
}
