import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MovieCreateComponent } from './components/movie-create/movie-create.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';
import { MovieComponent } from './movie.component';

const routes: Routes = [
    {
        path: '', component: MovieComponent, children: [
            { path: 'detail/:id', component: MovieDetailComponent },
            { path: 'create', component: MovieCreateComponent },
            { path: '**', pathMatch: 'full', redirectTo: '' }
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class MovieRoutingModule { }
