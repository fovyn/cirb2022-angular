import { FormArray, FormControl, FormGroup, Validators } from "@angular/forms";
import { postalCodeBelgium } from "src/app/modules/shared/validators/form-validator";

export function generateMovieForm(): FormGroup {
    return new FormGroup({
        titre: new FormControl(null, [Validators.required, postalCodeBelgium(4)]),
        duree: new FormControl(60, [Validators.required, Validators.min(60)]),
        actors: new FormArray([], [Validators.minLength(1)])
    })
}

export function generateActorForm(): FormGroup {
    return new FormGroup({
        firstname: new FormControl(null, [Validators.required, postalCodeBelgium(5)]),
        lastname: new FormControl(null, [Validators.required])
    })
}