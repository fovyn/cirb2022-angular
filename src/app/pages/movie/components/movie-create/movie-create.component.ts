import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { generateActorForm, generateMovieForm } from './movie-create.form';
import { FormArray } from '@angular/forms';

@Component({
    selector: 'movie-create',
    templateUrl: './movie-create.component.html',
    styleUrls: ['./movie-create.component.scss']
})
export class MovieCreateComponent implements OnInit {
    form: FormGroup = generateMovieForm();

    get Actors(): FormArray { return this.form.get("actors") as FormArray; }

    constructor() {
    }

    addActor() {
        const actorForm = generateActorForm();
        this.Actors.push(actorForm);
        console.log()
    }

    removeActor(index: number) {
        this.Actors.removeAt(index);
    }

    handleSubmit() {
        console.log(this.form.value);
    }

    ngOnInit(): void {
    }
}
