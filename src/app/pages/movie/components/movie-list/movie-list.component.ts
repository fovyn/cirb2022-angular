import { Component, Input, OnInit } from '@angular/core';
import { Pagination } from './../../../../modules/shared/components/pagination/pagination.model';

@Component({
    selector: 'movie-list',
    templateUrl: './movie-list.component.html',
    styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {
    @Input() blop: string | null;
    private movies: any[] = [];

    private pageInfo: Partial<Pagination> = { pageSize: 5, listSize: this.movies.length, currentPage: 1 };
    get PageInfo(): Partial<Pagination> { return this.pageInfo; }
    get StartIndex(): number { return (this.PageInfo.currentPage - 1) * this.PageInfo.pageSize; }
    get EndIndex(): number {
        return this.StartIndex + this.PageInfo.pageSize;
    }

    get Movies(): any[] { return this.movies; }

    constructor() { }

    handleChangePage(page: number) {
        this.pageInfo = { ... this.PageInfo, currentPage: page };
    }

    ngOnInit(): void {
    }

}