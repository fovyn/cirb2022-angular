import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MovieRoutingModule } from './movie-routing.module';
import { MovieComponent } from './movie.component';
import { MovieListComponent } from './components/movie-list/movie-list.component';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MovieCreateComponent } from './components/movie-create/movie-create.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        MovieComponent,
        MovieListComponent,
        MovieDetailComponent,
        MovieCreateComponent
    ],
    imports: [
        CommonModule,
        MovieRoutingModule,
        SharedModule,
        ReactiveFormsModule
    ]
})
export class MovieModule { }
