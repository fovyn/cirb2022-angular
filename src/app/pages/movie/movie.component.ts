import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'movie',
    templateUrl: './movie.component.html',
    styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
    selectedMovie: any | null = null;

    display(component: string) {
        switch (component) {
            case 'list': break;
            case 'detail': break;
        }
    }

    constructor() { }

    ngOnInit(): void {
    }

}
