import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChronoRoutingModule } from './chrono-routing.module';
import { ChronoComponent } from './chrono.component';
import { MaterializeModule } from 'src/app/modules/materialize/materialize.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';


@NgModule({
    declarations: [
        ChronoComponent
    ],
    imports: [
        CommonModule,
        ChronoRoutingModule,
        MaterializeModule,
        SharedModule
    ]
})
export class ChronoModule { }
