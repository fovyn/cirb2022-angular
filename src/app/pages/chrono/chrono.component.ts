import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'chrono',
    templateUrl: './chrono.component.html',
    styleUrls: ['./chrono.component.scss']
})
export class ChronoComponent implements OnInit, OnDestroy {
    private time: number = 0;
    get Time(): number { return this.time; }

    private intervalId: any;

    constructor() { }

    Start() {
        if (!this.intervalId) {
            this.intervalId = setInterval(() => this.time++, 1000);
        }
    }

    Stop() {
        clearInterval(this.intervalId);
        this.intervalId = null;
    }

    Clear() {
        this.Stop();
        this.time = 0;
    }


    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        if (this.intervalId) {
            clearInterval(this.intervalId);
        }
    }
}
