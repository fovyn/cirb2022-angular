import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'chrono'
})
export class ChronoPipe implements PipeTransform {

    transform(seconds: number): string {
        const minutes = parseInt(`${seconds / 60}`);

        return `${minutes} minutes ${seconds % 60} secondes`;
    }

}
