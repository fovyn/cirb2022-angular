import { AbstractControl, ValidatorFn } from "@angular/forms";

export function postalCodeBelgium(nbChar): ValidatorFn {

    return (control: AbstractControl) => {
        const value = control.value;

        console.log(value);
        if (value?.length == nbChar) {
            return null;
        }
        return { "postalCode": "Le code postal doit être de 4 caractère" };
    };
}
