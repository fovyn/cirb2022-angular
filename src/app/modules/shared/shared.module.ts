import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChronoPipe } from './pipes/chrono.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';



@NgModule({
    declarations: [
        ChronoPipe,
        PaginationComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        ChronoPipe,
        PaginationComponent
    ]
})
export class SharedModule { }
