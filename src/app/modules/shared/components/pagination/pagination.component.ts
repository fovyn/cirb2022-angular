import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pagination } from './pagination.model';

@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
    private pagination: Pagination = { pageSize: 5, currentPage: 0, listSize: 0 }
    get Pagination(): Pagination { return this.pagination; }
    @Input('pagination') set Pagination(v: Partial<Pagination>) {
        this.pagination = { ...this.pagination, ...v };
    }

    @Output("changePage") changePageEvent = new EventEmitter<number>();

    get Pages() {
        const a = [];
        const nbPage = this.LastPage;
        for (let i = 1; i <= nbPage; i++) {
            a.push(i);
        }
        return a;
    }

    get LastPage(): number {
        const value = this.Pagination.listSize / this.Pagination.pageSize;

        return value > parseInt(value.toString()) ? parseInt(value.toString()) + 1 : parseInt(value.toString());
    }

    constructor() { }

    ngOnInit(): void {
    }

    handlePreviousPage() {
        if (this.pagination.currentPage <= 1) return;
        this.changePageEvent.next(this.Pagination.currentPage - 1);
    }
    handleNextPage() {

        if (this.pagination.currentPage > this.LastPage - 1) return;
        this.changePageEvent.next(this.Pagination.currentPage + 1);
    }
    handlePage(page: number) {
        this.changePageEvent.next(page);
    }

}
