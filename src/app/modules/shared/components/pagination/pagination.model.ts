export interface Pagination {
    pageSize: number;
    currentPage: number;
    listSize: number;
}