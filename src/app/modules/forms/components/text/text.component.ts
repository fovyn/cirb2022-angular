import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NgControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { v4 as uuid } from 'uuid';

@Component({
    selector: 'form-input',
    templateUrl: './text.component.html',
    styleUrls: ['./text.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            multi: true,
            useExisting: forwardRef(() => TextComponent)
        },

    ]
})
export class TextComponent implements OnInit, ControlValueAccessor {
    private uuid: string;
    get Id(): string { return this.uuid; }

    private label: string;
    get Label(): string { return this.label; }
    @Input("label") set Label(v: string) { this.label = v; }

    private errors: Array<{ key: string, message: string }> = [];
    get Errors(): Array<{ key: string, message: string }> { return this.errors; }
    @Input("errors") set Errors(v: Array<{ key: string, message: string }>) { this.errors = v; }

    value: any;
    onChange: any = (value) => { };
    onTouched: any = () => { };

    constructor(

    ) {
        this.uuid = uuid();
    }
    writeValue(obj: any): void {
        this.value = obj;
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }
    setDisabledState?(isDisabled: boolean): void {

    }

    ngOnInit(): void {
    }


    change(input: any) {
        this.value = input.value;
        this.onChange(input.value);
    }
}
