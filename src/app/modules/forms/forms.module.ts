import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextComponent } from './components/text/text.component';


import { FormsModule as NgFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        TextComponent
    ],
    imports: [
        CommonModule,
        NgFormsModule
    ],
    exports: [
        NgFormsModule,
        TextComponent
    ]
})
export class FormsModule { }
