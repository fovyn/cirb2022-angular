import { FormControl, FormGroup, Validators } from "@angular/forms";

export function LoginForm(): FormGroup {
    return new FormGroup({
        email: new FormControl("flavian.ovyn@bstorm.be", [Validators.required, Validators.email]),
        password: new FormControl(null, [Validators.required, Validators.minLength(4)])
    })
}