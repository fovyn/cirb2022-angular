import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { LoginForm } from './login.form';
import { FormGroup } from '@angular/forms';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup = LoginForm();

    constructor(private $auth: AuthService) { }

    ngOnInit(): void {
    }

    handleLogin() {
        console.log(this.loginForm.value);
        if (this.loginForm.valid) {
            const { email, password } = this.loginForm.value;
            console.log(email, password);
            this.$auth.Login(email, password);
        }
    }

}
