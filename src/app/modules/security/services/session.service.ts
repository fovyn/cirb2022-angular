import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, map } from 'rxjs';
import jwtDecode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    private token$ = new BehaviorSubject<string | null>(null);

    get Token$(): Observable<string | null> { return this.token$.asObservable(); }

    constructor() {
        const token: string | null = localStorage.getItem("_t");
        if (token) {
            this.token$.next(token);
        }
    }

    private Decode(token: string, field: string): string | null {
        if (!token) return null;
        return jwtDecode(token)[field];
    }

    get Email(): string | null {
        if (!this.token$.value) return null;
        return this.Decode(this.token$.value, 'email');
    }

    get Email$(): Observable<string | null> {
        return this.Token$.pipe(map(it => this.Decode(it, 'email')));
    }

    get IsExpire$(): Observable<boolean | null> {
        return this.Token$.pipe(map(it => it && new Date(this.Decode(it, 'exp')) < new Date() || null))
    }

    SignIn(token: string): string | null {
        this.token$.next(token);
        localStorage.setItem("_t", JSON.stringify(token));
        return this.Email;
    }

    SignOut() {
        this.token$.next(null);
        localStorage.removeItem("_t");
    }

    CheckIfExpire(): boolean {
        if (this.token$.value) {
            return new Date(this.Decode(this.token$.value, 'exp')) < new Date()
        } else {
            return false;
        }
    }
}
