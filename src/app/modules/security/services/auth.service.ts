import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SessionService } from './session.service';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private email: string;
    private password: string;

    constructor(
        private $http: HttpClient,
        private $session: SessionService
    ) {
    }

    Register(user: any): Observable<any> {
        this.$http.post<any>(`${environment.api.url}/register`, { ...user })
            .subscribe({
                next: (res) => { this.$session.SignIn(res); },
                error: (err) => { console.log("ERROR"); }
            });

        return this.$session.Token$;
    }

    Login(email: string, password: string): Observable<any> {
        this.email = email;
        this.password = password;

        this.$http.post<any>(`${environment.api.url}/login`, { email, password })
            .subscribe({
                next: (res) => { this.$session.SignIn(res.accessToken); },
                error: (err) => { console.log(err); }
            });

        return this.$session.Token$;
    }
}
