import { Directive, Input, ElementRef, OnInit, OnDestroy } from '@angular/core';
import * as M from "materialize-css";

@Directive({
    selector: '[matModal]'
})
export class MatModalDirective implements OnInit, OnDestroy {
    private mInstance: M.Modal;
    get MInstance(): M.Modal { return this.mInstance; }
    set MInstance(v: M.Modal) { this.mInstance = v; }

    private options: Partial<M.ModalOptions>;
    get Options(): Partial<M.ModalOptions> { return this.options; }
    @Input("options") set Options(v: Partial<M.ModalOptions>) { this.options = { ...v }; }

    get Target(): HTMLElement { return this.$elRef.nativeElement; }


    constructor(private $elRef: ElementRef<HTMLElement>) { }

    ngOnInit(): void {
        this.MInstance = M.Modal.init(this.Target, this.Options);
    }
    ngOnDestroy(): void {
        this.MInstance.destroy();
    }

    Open() {
        if (!this.MInstance.isOpen) {
            this.MInstance.open();
        }
    }

    Close() {
        if (this.MInstance.isOpen) {
            this.MInstance.close();
        }
    }
}
