import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector: '[matFloatingActionButton]'
})
export class MatFloatingActionButtonDirective implements OnInit {
    private mInstance: M.FloatingActionButton;

    get Element(): HTMLDivElement { return this.$elRef.nativeElement; }

    constructor(private $elRef: ElementRef<HTMLDivElement>) { }

    ngOnInit(): void {
        this.mInstance = M.FloatingActionButton.init(this.Element);
    }

}
