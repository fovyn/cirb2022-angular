import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatModalDirective } from './directives/mat-modal.directive';
import { MatFloatingActionButtonDirective } from './directives/mat-floating-action-button.directive';



@NgModule({
    declarations: [
        MatModalDirective,
        MatFloatingActionButtonDirective
    ],
    imports: [
        CommonModule
    ],
    exports: [
        MatModalDirective,
        MatFloatingActionButtonDirective
    ]
})
export class MaterializeModule { }
