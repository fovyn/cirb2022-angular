import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/modules/security/services/session.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'nav-bar',
    templateUrl: './nav-bar.component.html',
    styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
    get Email$(): Observable<string | null> { return this.$session.Email$; }

    constructor(
        private $session: SessionService
    ) { }

    ngOnInit(): void {
    }

    logout() {
        this.$session.SignOut();
    }
}
