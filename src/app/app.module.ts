import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { DecimalPipe } from '@angular/common';
import { SharedModule } from './modules/shared/shared.module';
import { MaterializeModule } from './modules/materialize/materialize.module';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HomeComponent } from './components/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http'
import { SecurityModule } from './modules/security/security.module';

@NgModule({
    declarations: [
        AppComponent,
        NavBarComponent,
        HomeComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        SharedModule,
        MaterializeModule,
        HttpClientModule,
        SecurityModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
