import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { MovieService } from './services/movie.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    genres$: Observable<{ value: any[], loading: boolean }>

    constructor(
        public $movie: MovieService
    ) {
        this.genres$ = $movie.State$;
    }

    ngOnInit(): void {
        // this.genres$ = 
    }

    loadGenres() {
        this.$movie.GetGenres();
    }
}
